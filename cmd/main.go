package main

import (
	"context"
	"log"
	"os"

	"github.com/OybekNarzullaev/go-blog-app/adabters/services"
	"github.com/OybekNarzullaev/go-blog-app/adabters/storage"
	"github.com/OybekNarzullaev/go-blog-app/controllers"
	"github.com/OybekNarzullaev/go-blog-app/ports/grpc"
	"github.com/OybekNarzullaev/go-blog-app/ports/rest"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
)

// @title           Go Blog App Api
// @version         v1.0
// @description     This is a sample server celler server.
// @termsOfService  http://swagger.io/terms/

// @contact.name   Oybek Narzullaev
// @contact.url    http://www.swagger.io/support
// @contact.email  oybeknarzullaev99@gmail.com

// @host      localhost:8080
// @BasePath  /api/v1

// @securityDefinitions.basic  Authorization

func main() {
	/*--------------load config files and env variables--------------------*/ 
	if err := initConfig(); err != nil {
		log.Fatalf("Error initializing config files: %s",err.Error())
	}

	if err := godotenv.Load(); err != nil {
		log.Fatalf("Error initializing environment variables: %s",err.Error())
	}
	
	/*--------------configurate db------------------------*/ 
	db, err := storage.NewDBConfig(storage.Config{
		Host:     viper.GetString("db.host"),
		Port:     viper.GetString("db.port"),
		Username: viper.GetString("db.username"),
		Password: os.Getenv("DB_PASSWORD"),
		DBName:   viper.GetString("db.db_name"),
		SSLMode:  viper.GetString("db.ssl_mode"),
	})
	if err != nil {
		log.Fatalf("Error initializing db: %s",err.Error())
	}

	/*-------------------buisness logic-------------------*/ 
	storages := storage.NewStorage(db)
	services := services.NewService(storages)
	controllers := controllers.NewHandler(services)
	handlers := rest.NewRouter(controllers)

	/*-------------------build server object--------------*/ 
	server := new(grpc.Server)
	// run server
	go func ()  {
		if err := server.Run(viper.GetString("port"), handlers.InitRoutes()); err != nil {
		log.Fatalf("Error occured while running http server: %s", err.Error())
	}
	}()
	
	log.Println("Server started...")

	quit := make(chan os.Signal, 1)
	<-quit
	log.Println("Server stopped...")

	// shut down server
	if err := server.Shudown(context.Background()); err != nil {
		log.Fatalf("Error occured on server shutting down: %s", err.Error())
	}
}

// A function for read from config files
func initConfig() error {
	viper.AddConfigPath("config")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}