package services

import (
	"github.com/OybekNarzullaev/go-blog-app/adabters/storage"
	"github.com/OybekNarzullaev/go-blog-app/domains/models"
)

type Authorization interface {
	CreateUser(input models.SignUpInput) (int, error)
	GenerateToken(username, password string) (string, error)
}

type Posts interface {
	Create(userId int, title, description string) (int, error)
	GetAll(userId int) ([]models.Post, error)
	GetById(userId, postId int) (models.Post, error)
	GetByUserId(userId, urlUserId int) ([]models.Post, error)
	Update(userId, postId int, input models.InputPost) error
	Delete(userId, postId int) error
}

type Service struct {
	Authorization
	Posts
}

func NewService(repo *storage.Storage) *Service {
	return &Service{
		Authorization: NewAuthService(repo.Authorization),
		Posts: NewPostService(repo.Posts, repo.Users),
	}
}

