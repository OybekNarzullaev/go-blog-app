package services

import (
	"time"

	"github.com/OybekNarzullaev/go-blog-app/adabters/storage"
	"github.com/OybekNarzullaev/go-blog-app/domains/models"
	"github.com/OybekNarzullaev/go-blog-app/utils"
	"github.com/dgrijalva/jwt-go"
)

type AuthService struct {
	repo storage.Authorization
}

type tokenClaims struct {
	jwt.StandardClaims
	UserId int `json:"user_id"`
}
func NewAuthService(repo storage.Authorization) *AuthService{
	return &AuthService{repo: repo}
}

func (s *AuthService) CreateUser(user models.SignUpInput) (int, error) {
	user.Password = utils.GenerateHashPassword(user.Password)
	return s.repo.CreateUser(user)
}

func (s* AuthService) GenerateToken(username, password string) (string, error) {
	user, err := s.repo.GetCurrentUser(username, utils.GenerateHashPassword(password))
	if err != nil {
		return "", err
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &tokenClaims{
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(12 * time.Hour).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
		user.Id,
	})

	return token.SignedString([]byte("salom"))
}