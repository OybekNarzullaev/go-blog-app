package services

import (
	"github.com/OybekNarzullaev/go-blog-app/adabters/storage"
	"github.com/OybekNarzullaev/go-blog-app/domains/models"
)

type PostService struct {
	repoPost storage.Posts
	repoUser storage.Users
}

func NewPostService(repoPost storage.Posts, repoUser storage.Users) *PostService {
	return &PostService{repoPost: repoPost,repoUser: repoUser}
}

/*-------------methods--------------------*/
func(s *PostService) Create(userId int, title, desc string) (int, error) {
	// check user
	_, err := s.repoUser.GetUserById(userId)
	if err != nil {
		return 0, err
	}

	// pass to repository
	id, err := s.repoPost.Create(userId, title, desc)
	
	if err != nil {
		return 0, err
	}

	// ok
	return id, nil
}

func (s *PostService) GetAll(userId int) ([]models.Post, error) {
	// check user
	_, err := s.repoUser.GetUserById(userId)
	if err != nil {
		return nil, err
	}
	// get all posts from repository
	return s.repoPost.GetAll()
}

func (s *PostService) GetById(userId, postId int) (models.Post, error) {
	// chech user Again
	_, err := s.repoUser.GetUserById(userId)
	if err != nil {
		return models.Post{}, err
	}

	// get post from repository
	return s.repoPost.GetById(postId)
}

func (s *PostService) GetByUserId(userId, urlUserId int) ([]models.Post, error) {
	// check user
	_, err := s.repoUser.GetUserById(userId)
	if err != nil {
		return nil, err
	}

	// get posts by userId
	return s.repoPost.GetByUserId(urlUserId)
}

func (s *PostService) Update(userId, postId int, input models.InputPost) error {
	// check user
	_, err := s.repoUser.GetUserById(userId)
	if err != nil {
		return err
	}

	// update post in db
	return s.repoPost.Update(postId, input)
}

func (s *PostService) Delete(userId, postId int) error {
	// check user
	_, err := s.repoUser.GetUserById(userId)
	if err != nil {
		return err
	}

	// update post in db
	return s.repoPost.Delete(postId)
}