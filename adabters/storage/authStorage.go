package storage

import (
	"fmt"

	"github.com/OybekNarzullaev/go-blog-app/domains/models"
	"github.com/jmoiron/sqlx"
)

type AuthStorage struct {
	db *sqlx.DB
}

func NewAuthStorage(db *sqlx.DB) *AuthStorage {
	return &AuthStorage{db: db}
}

func (r *AuthStorage) CreateUser(user models.SignUpInput)(int, error){
	var id int
	// query string
	query := fmt.Sprintf("INSERT INTO %s (name, username, password) VALUES ($1, $2, $3) RETURNING id", userTable)

	// send query to db
	row := r.db.QueryRow(query, user.Name, user.Username, user.Password)

	// scan id
	if err := row.Scan(&id); err != nil {
		return 0, err
	}

	// success
	return id, nil
}

func (r *AuthStorage) GetCurrentUser(username, passwordHash string) (models.User, error) {
	var user models.User
	
	// write query
	query := fmt.Sprintf(`SELECT id FROM %s WHERE username=$1 AND password=$2`, userTable)

	// get user
	err := r.db.Get(&user, query, username, passwordHash)
	return user, err
}
