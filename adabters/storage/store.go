package storage

import (
	"github.com/OybekNarzullaev/go-blog-app/domains/models"
	"github.com/jmoiron/sqlx"
)

type Authorization interface {
	CreateUser(user models.SignUpInput) (int, error)
	GetCurrentUser(username, passwordHash string) (models.User, error)
}

type Posts interface {
	Create(userId int, title, desc string) (int, error)
	GetAll() ([]models.Post, error)
	GetById(postId int) (models.Post, error) 
	GetByUserId(userId int) ([]models.Post, error)
	Update(postId int, input models.InputPost) error
	Delete(postId int) error
}

type Users interface {
	GetUserById(userId int) (models.User, error)
}

type Storage struct {
	Authorization
	Posts
	Users
}

func NewStorage(db *sqlx.DB) *Storage {
	return &Storage{
		Authorization: NewAuthStorage(db),
		Posts: NewPostStorage(db),
		Users: NewUserStorage(db),
	}
}