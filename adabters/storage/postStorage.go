package storage

import (
	"fmt"
	"strings"

	"github.com/OybekNarzullaev/go-blog-app/domains/models"
	"github.com/jmoiron/sqlx"
)

type PostStorage struct {
	db *sqlx.DB
}

func NewPostStorage(db *sqlx.DB) *PostStorage {
	return &PostStorage{db: db}
}

func (r *PostStorage) Create(userId int, title, desc string) (int, error) {
	// begin transaction
	tx, err := r.db.Begin()
	if err != nil {
		return 0, nil
	}

	var postId int
	// write query for create post
	createPost := fmt.Sprintf(`INSERT INTO %s (title, description) VALUES($1, $2) RETURNING id`, postTable)
	row := tx.QueryRow(createPost, title, desc)

	
	// scan row
	if err := row.Scan(&postId); err != nil {
		tx.Rollback()
		return 0, err
	}

	//write query for create postList
	createPostList := fmt.Sprintf(`INSERT INTO %s (post_id, user_id) VALUES($1, $2)`, postListTable)
	_, err = tx.Exec(createPostList, postId, userId)
	if err != nil {
		tx.Rollback()
		return 0, err
	}

	// ok
	return postId, tx.Commit()
}

func (r *PostStorage) GetAll() ([]models.Post, error) {
	var posts []models.Post
	
	// write query
	query := fmt.Sprintf(`SELECT p.id, p.title, p.description FROM %s p JOIN %s l ON p.id=l.post_id`, postTable, postListTable)

	// send db
	err := r.db.Select(&posts, query)
	if err != nil {
		return nil, err
	}

	// ok 
	return posts, nil	
}

func (r *PostStorage) GetById(postId int) (models.Post, error) {
	// write query
	query := fmt.Sprintf(`SELECT p.id, p.title, p.description FROM %s p JOIN %s l ON p.id=l.post_id WHERE p.id=$1`, postTable, postListTable)
	
	// get from db
	var post models.Post
	err := r.db.Get(&post, query, postId)
	if err != nil {
		return models.Post{}, err
	}

	// ok
	return post, nil
}

func (r *PostStorage) GetByUserId(userId int) ([]models.Post, error) {
	// write query
	query := fmt.Sprintf(`SELECT p.id, p.title, p.description FROM %s p JOIN %s l ON p.id=l.post_id WHERE l.user_id=$1`, postTable, postListTable)

	// get from db
	var posts []models.Post
	err := r.db.Select(&posts, query, userId)
	if err != nil {
		return nil, err
	}

	// ok
	return posts, nil
}

func (r *PostStorage) Update(postId int, input models.InputPost) error {
	// helper slices
	setValues := make([]string, 0)
	args := make([]interface{}, 0)
	argsId := 1

	// validate input
	if input.Title != "" {
		setValues = append(setValues, fmt.Sprintf("title=$%d", argsId))
		args = append(args, input.Title)
		argsId++
	}

	if input.Description != "" {
		setValues = append(setValues, fmt.Sprintf("description=$%d", argsId))
		args = append(args, input.Description)
		argsId++
	}

	setQuery := strings.Join(setValues, ", ")

	// write question
	query := fmt.Sprintf(`UPDATE %s SET %s WHERE id=%d`, postTable, setQuery, postId)

	_, err := r.db.Exec(query, args...)
	return err
}

func (r *PostStorage) Delete(userId int) error {
	// write question
	query := fmt.Sprintf(`DELETE FROM %s WHERE id=$1`, postTable)
	_, err := r.db.Exec(query, userId)
	return err
}