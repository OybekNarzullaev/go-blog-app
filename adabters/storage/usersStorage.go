package storage

import (
	"fmt"

	"github.com/OybekNarzullaev/go-blog-app/domains/models"
	"github.com/jmoiron/sqlx"
)

type UserStorage struct {
	db *sqlx.DB
}

func NewUserStorage(db *sqlx.DB) *UserStorage {
	return &UserStorage{db: db}
}

func (r *UserStorage) GetUserById(userId int) (models.User, error) {
	var user models.User
	
	// write query
	query := fmt.Sprintf(`SELECT id, name, username, password FROM %s`, userTable)
	
	// send query
	err := r.db.Get(&user, query)
	
	// return values
	return user, err
}