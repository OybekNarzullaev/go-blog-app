package storage

import (
	"fmt"

	"github.com/jmoiron/sqlx"
)

type Config struct {
	Host     string
	Port     string
	Username string
	Password string
	DBName   string
	SSLMode  string
}

func NewDBConfig(cfg Config) (*sqlx.DB, error) {
	// string to congigurate db
	db, err := sqlx.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s password=%s sslmode=%s",
		cfg.Host,
		cfg.Port,
		cfg.Username,
		cfg.Password,
		cfg.SSLMode,
	))
	if err != nil {
		return nil, err
	}

	// ping db
	if err := db.Ping(); err != nil {
		return nil, err
	}
	// all are okay!
	return db, nil
}

const (
	userTable = "users"
	postTable = "posts"
	postListTable = "posts_list"
)