package controllers

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/OybekNarzullaev/go-blog-app/adabters/services"
	"github.com/OybekNarzullaev/go-blog-app/domains/models"
	"github.com/OybekNarzullaev/go-blog-app/utils"
	"github.com/gin-gonic/gin"
)

type PostController struct {
	services services.Posts
}

func NewPostController(services services.Posts)*PostController {
	return &PostController{services: services}
}

func (h *PostController) Create(c *gin.Context) {
	// get user id
	userId, err := GetUserId(c)
	if err != nil {
		utils.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	// bind json
	var input models.InputPost
	if err := c.BindJSON(&input); err != nil {
		utils.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	// pass to setvice
	id, err := h.services.Create(userId, input.Title, input.Description)
	if err != nil {
		utils.NewErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	// ok
	c.JSON(http.StatusOK, map[string]interface{}{
		"id": id,
	})
}

func (h *PostController) GetAll(c *gin.Context) {
	// get userId from middleware function
	userId, err := GetUserId(c)
	if err != nil {
		utils.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	// get all posts
	var posts []models.Post

	// get posts from service
	posts, err = h.services.GetAll(userId)
	if err != nil {
		utils.NewErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}
	
	c.JSON(http.StatusOK, map[string]interface{}{
		"posts": posts,
	})
}

func (h *PostController) GetById(c *gin.Context) {
	// check userId
	userId, err := GetUserId(c)
	if err != nil {
		utils.NewErrorResponse(c, http.StatusUnauthorized, err.Error())
		return
	}

	// get param
	postId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		utils.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}
	// get post from service
	var post models.Post
	post, err = h.services.GetById(userId, postId)
	if err != nil {
		utils.NewErrorResponse(c, http.StatusUnauthorized, err.Error())
		return
	}

	// ok
	c.JSON(http.StatusOK, map[string]interface{}{
		"post": post,
	})
}

func (h *PostController) GetByUserId(c *gin.Context) {
	// get userId
	userId, err := GetUserId(c)
	if err != nil {
		utils.NewErrorResponse(c, http.StatusUnauthorized, err.Error())
		return
	}

	// get post id from params
	urlUserId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		utils.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	// get posts
	var posts []models.Post
	posts, err = h.services.GetByUserId(userId, urlUserId)
	if err != nil {
		utils.NewErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"user_id": urlUserId,
		"posts": posts,
	})
}

func (h *PostController) Update(c *gin.Context) {
	// get userId
	userId, err := GetUserId(c)
	if err != nil {
		utils.NewErrorResponse(c, http.StatusUnauthorized, err.Error())
		return
	}

	// get post id from url param
	postId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		utils.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	// get input from request body
	var input models.InputPost
	err = c.BindJSON(&input)
	if err != nil {
		utils.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	// pass to services
	if err := h.services.Update(userId, postId, input); err != nil {
		utils.NewErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"id": postId,
		"message": "updated",
	})
}

func (h *PostController) Delete(c *gin.Context) {
	// get userId
	userId, err := GetUserId(c)
	if err != nil {
		utils.NewErrorResponse(c, http.StatusUnauthorized, err.Error())
		return
	}

	// get post id from url param
	postId, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		utils.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	// pass to services
	if err := h.services.Delete(userId, postId); err != nil {
		utils.NewErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	c.JSON(http.StatusOK, map[string]interface{}{
		"id": postId,
		"message": "deleted",
	})
}

// get userId
func GetUserId(c *gin.Context) (int, error) {
	id, ok := c.Get("userId")
	if !ok {
		return 0, errors.New("user id not found")
	}
	
	idInt, ok := id.(int)
	if !ok {
		return 0, errors.New("user id is of invalid type")
	}

	return idInt, nil
}