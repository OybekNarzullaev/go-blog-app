package controllers

import (
	"github.com/OybekNarzullaev/go-blog-app/adabters/services"
	"github.com/gin-gonic/gin"
)

type Authorization interface{
	SignUp(c *gin.Context)
	SignIn(c *gin.Context)
}

type Posts interface{
	Create(c *gin.Context)
	GetAll(c *gin.Context)
	GetById(c *gin.Context)
	GetByUserId(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
}

type Controller struct {
	Authorization
	Posts
}

func NewHandler(services *services.Service) *Controller {
	return &Controller{
		Authorization:NewAuthController(services.Authorization),
		Posts:  NewPostController(services.Posts),
	}}

