package controllers

import (
	"net/http"

	"github.com/OybekNarzullaev/go-blog-app/adabters/services"
	"github.com/OybekNarzullaev/go-blog-app/domains/models"
	"github.com/OybekNarzullaev/go-blog-app/utils"
	"github.com/gin-gonic/gin"
)

type AuthController struct {
	services services.Authorization
}

func NewAuthController(services services.Authorization) *AuthController {
	return &AuthController{services: services}
}

func (h *AuthController) SignUp(c *gin.Context) {
	// binding with json
	var input models.SignUpInput
	if err := c.BindJSON(&input); err != nil {
		utils.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	// pass to services
	id, err := h.services.CreateUser(input)
	if err != nil {
		utils.NewErrorResponse(c, http.StatusInternalServerError, err.Error())
		return
	}

	// all are success
	c.JSON(http.StatusOK, map[string]interface{}{
		"id": id,
		"message": "Successfully",
	})
}

func (h *AuthController) SignIn(c *gin.Context) {
	var input models.SignInInput
	
	// binding json
	if err := c.BindJSON(&input); err != nil {
		utils.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	// generate token in service
	token, err := h.services.GenerateToken(input.Username, input.Password)
	if err != nil {
		utils.NewErrorResponse(c, http.StatusBadRequest, err.Error())
		return
	}

	// succes
	c.JSON(http.StatusOK, map[string]interface{}{
		"token": token,
	})

}

// set middleware
