package utils

import (
	"crypto/sha1"
	"fmt"
	"log"
	"os"

	"github.com/gin-gonic/gin"
)

func NewErrorResponse(c *gin.Context, statusCode int, message string) {
	log.Println(message)
	c.AbortWithStatusJSON(statusCode, map[string]interface{}{
		"message": message,
	})
}

func GenerateHashPassword(password string) string {
	hash := sha1.New()
	hash.Write([]byte(password))
	salt := os.Getenv("SALT")
	return fmt.Sprintf("%x", hash.Sum([]byte(salt)))
}
