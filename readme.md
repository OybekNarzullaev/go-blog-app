# commites

1. Setup server
2. Setup routers and handlers
3. Full build structure of project
4. Initial first config files
5. Migrations
   docker run --name blog_db -e POSTGRES_PASSWORD=<????> -p 5437:5432 -d postgres
   migrate -path ./migrations -database 'postgres://postgres:<????>@localhost:5437/postgres?sslmode=disable' up
   migrate -path ./migrations -database 'postgres://postgres:<????>@localhost:5437/postgres?sslmode=disable' down
6. Connected to db
7. Register user
8. Login with jwt
9. Middleware
10. Create post
11. Get All posts
12. Get One post
13. Get Posts By userId
14. Update Post
15. Delete Post
16. Add some swagger
17. Add docker and docker-compose
18. Add database directory to .gitignore
19. Move routers to rest package
