CREATE TABLE users 
( 
    id bigserial NOT NULL PRIMARY KEY, 
    name varchar(255) NOT NULL,
    password varchar(255) NOT NULL, 
    username varchar(255) NOT NULL UNIQUE, 
    created_at timestamp NOT NULL DEFAULT NOW(), 
    updated_at timestamptz
);

CREATE TABLE posts
( 
    id bigserial NOT NULL PRIMARY KEY, 
    title varchar(2255) NOT NULL, 
    description varchar(255) NOT NULL, 
    created_at timestamp NOT NULL DEFAULT NOW(), 
    updated_at timestamptz
);

CREATE TABLE posts_list
(
    id bigserial NOT NULL PRIMARY KEY, 
    post_id int REFERENCES posts (id) ON DELETE CASCADE NOT NULL, 
    user_id int REFERENCES users (id) ON DELETE CASCADE NOT NULL, 
    created_at timestamp NOT NULL DEFAULT NOW(), 
    updated_at timestamptz
)