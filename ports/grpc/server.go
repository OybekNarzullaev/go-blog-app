package grpc

import (
	"context"
	"net/http"
	"time"
)

// server type
type Server struct {
	httpServer *http.Server
}

// run server function
func (s *Server) Run(port string, handler http.Handler) error {
	s.httpServer = &http.Server{
		Addr: ":" + port,
		Handler: handler,
		ReadTimeout: 10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	return s.httpServer.ListenAndServe()
}

// shutdown server
func (s *Server) Shudown(ctx context.Context) error {
	return s.httpServer.Shutdown(ctx)
}
