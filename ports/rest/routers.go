package rest

import (
	"github.com/OybekNarzullaev/go-blog-app/controllers"

	ginSwagger "github.com/swaggo/gin-swagger"

	_ "github.com/OybekNarzullaev/go-blog-app/docs"
	"github.com/OybekNarzullaev/go-blog-app/utils"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

type Router struct {
	controllers *controllers.Controller
}

func NewRouter(controllers *controllers.Controller) *Router {
	return &Router{controllers: controllers}
}

func (r *Router) InitRoutes() *gin.Engine {
	router := gin.Default()
	router.Use(cors.Default())
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	auth := router.Group("/auth") 
	{
		auth.POST("/sign-up", r.controllers.Authorization.SignUp)
		auth.POST("/sign-in",  r.controllers.Authorization.SignIn)
	}
	api := router.Group("/api", r.userIndentify)
	{
		posts := api.Group("/posts")
		{
			posts.POST("/", r.controllers.Posts.Create)
			posts.GET("/", r.controllers.Posts.GetAll)
			posts.GET("/:id", r.controllers.Posts.GetById)
			posts.GET("by-user/:id", r.controllers.Posts.GetByUserId)
			posts.PUT("/:id", r.controllers.Posts.Update)
			posts.DELETE("/:id", r.controllers.Posts.Delete)
		}
	}
	return router
}

func (h *Router) userIndentify(c *gin.Context) {
	userId, statusCode, errMess := UserIndentify(c.GetHeader("Authorization"))
	if errMess != "" {
		utils.NewErrorResponse(c, statusCode, errMess)
		return
	}

	c.Set("userId", userId)
}