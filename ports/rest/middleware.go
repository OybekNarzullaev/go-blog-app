package rest

import (
	"errors"
	"net/http"
	"strings"

	"github.com/dgrijalva/jwt-go"
)


type tokenClaims struct {
	jwt.StandardClaims
	UserId int `json:"user_id"`
}

func UserIndentify(header string) (int, int, string) {
	// get header of request
	if header == "" {
		return 0, http.StatusUnauthorized, "Empty auth header"
	}

	// header parts
	headerParth := strings.Split(header, " ")
	if len(headerParth) != 2 {
		return  0, http.StatusUnauthorized, "Invalid auth header"
	}

	// check token
	if len(headerParth[1]) == 0 {
		return 0, http.StatusUnauthorized, "Empty token"
	}

	// parse token
	accessToken := headerParth[1]
	token, err := jwt.ParseWithClaims(accessToken, &tokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("invalid signing method")
		}

		return []byte("salom"), nil
	})

	if err != nil {
		return 0, http.StatusInternalServerError, "Invalid signing method"
	}
	
	claims, ok := token.Claims.(*tokenClaims)
	if !ok {
		return 0, http.StatusInternalServerError, "Token claims are not type tokenClaims"
	}

	return claims.UserId, -1, ""
}

