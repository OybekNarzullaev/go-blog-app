package models

type Post struct {
	Id          int    `json:"id" db:"id"`
	Title       string `json:"title" db:"title"`
	Description string `json:"description" db:"description"`
	//CreatedAt   string `json: "created_at" db:"created_at"`
	//UpdatedAt   string `json: "updated_at" db:"updated_at"`
}

type PostList struct {
	Id     int `json:"id" db:"id"`
	PostId int `json: "post_id" db:"post_id"`
	UserId int `json: "user_id" db: "user_id"`
	//CreatedAt time.Time    `json: "created_at" db:"created_at"`
	//UpdatedAt time.Time `json: "updated_at" db:"updated_at"`
}

type InputPost struct {
	Title       string `json:"title" db:"title" binding:"required"`
	Description string `json:"description" db:"description" binding:"required"`
}